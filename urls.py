# -*- coding: utf-8 -*-

from django.conf.urls import patterns

urlpatterns = patterns(
    'apps.niobe.views',
    (r'^course/(?P<course_id>\d+)/subjects/?$', 'course_subjects'),
    (r'^course/(?P<course_id>\d+)/subject/(?P<subject_id>\d+)/?$', 'subject_show'),
    
    (r'^teacher/show/?$', 'teacher_list'),

    (r'', 'index'),
)
