# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, InvalidPage

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q, Sum
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.core.urlresolvers import reverse

from apps.niobe.forms import SearchForm, SubjectToTeacherInlineFormset
from apps.merovingian.models import SubjectToTeacher, Subject, Course, MerovingianAdmin, Module
from apps.trainman.models import Teacher
from apps.merovingian.functions import user_subjects, make_page, user_courses

# UTILS


def user_course_subjects(user, course):
    try:
        admin = MerovingianAdmin.objects.get(user_profile__user=user)
    except MerovingianAdmin.DoesNotExist:
        admin = None

    if user.is_superuser or (admin is not None and admin.temporary_privileged_access):
        subjects = Subject.objects.all()
    else:
        subjects = Subject.objects.didactic_offer_and_future()

    return subjects.filter(
        module__in=Module.objects.active().filter(
            sgroup__course=course,
            sgroup__is_active=True))


@login_required
def index(request):
    # Getting courses matching criteria
    courses = user_courses(request.user)

    # Search form
    name = ''
    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            name = request.session['niobe_courses_search'] = search_form.cleaned_data['name']
    else:
        name = request.session.get('niobe_courses_search', '')
        search_form = SearchForm(initial={'name': name})
    if name != '':
        courses = courses.filter(name__istartswith=name)

    # Pagination
    paginator = Paginator(courses, 25)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        courses = paginator.page(page)
    except (EmptyPage, InvalidPage):
        courses = paginator.page(paginator.num_pages)

    kwargs = {'courses': courses, 'search_form': search_form}
    return render_to_response('niobe/index.html', kwargs, context_instance=RequestContext(request))


@login_required
def course_subjects(request, course_id):

    try:
        course = user_courses(request.user).get(pk=course_id)
    except Course.DoesNotExist:
        return HttpResponseNotFound()

    subjects = user_course_subjects(request.user, course)

    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            name = request.session['niobe_subject_search'] = search_form.cleaned_data['name']
    else:
        name = request.session.get('niobe_subject_search', '')
        search_form = SearchForm(initial = {'name': name})

    for n in name.split(','):
        n = n.strip()
        subjects = subjects.filter(Q(name__istartswith = n) | 
                                   Q(type__name__istartswith = n) | 
                                   Q(assessment__name__istartswith = n) |
                                   Q(module__sgroup__name__istartswith = n) |
                                   Q(module__sgroup__course__name__istartswith = n) |
                                   Q(module__sgroup__course__level__name__istartswith = n) |
                                   Q(teachers__user_profile__user__last_name__istartswith = n)).distinct()
                                   
    subject_page = make_page(request, subjects, 'niobe_subject_teacher')
    
    subject_teacher = [
        {
            'subject': subject,
            'subject_to_teacher': SubjectToTeacher.objects.filter(subject = subject).order_by('teacher__user_profile__user__last_name')
        } 
        for subject in subject_page
    ]
    
    kwargs = {'course': course,
              'subject_page': subject_page,
              'subject_teacher_list': subject_teacher,
              'search_form': search_form}
    return render_to_response('niobe/course_subjects.html',
                              kwargs,
                              context_instance = RequestContext(request))

@login_required
@permission_required('merovingian.change_subject')
def subject_show(request, course_id, subject_id):
    try:
        course = user_courses(request.user).get(pk=course_id)
        subjects = user_course_subjects(request.user, course)
        subject = subjects.get(id=subject_id)
        if request.method == 'POST':
            subject_to_teacher_formset = SubjectToTeacherInlineFormset(request.POST, request.FILES, instance = subject)        
            if subject_to_teacher_formset.is_valid():
                subject_to_teacher_formset.save()
                messages.success(request, _(u'Subject %s has been saved succesfully.') % subject)
            else:
                messages.error(request, _(u'Correct errors listed below.'))
        else:
            subject_to_teacher_formset = SubjectToTeacherInlineFormset(instance = subject)
    except Course.DoesNotExist:
        return HttpResponseNotFound()
    except Subject.DoesNotExist:
        messages.error(request, _(u'Selected subject does not exist. If you see this message again, contact the Administrator.') + ' #s0e1')
        return redirect('apps.niobe.views.index')
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #s0e0')
        return redirect('apps.niobe.views.index')
    else:
        if request.POST.get('action') == 'save' and subject_to_teacher_formset.is_valid():
            return redirect('apps.niobe.views.course_subjects', course_id = course.id)
        elif request.POST.get('action') == 'save_and_edit' and subject_to_teacher_formset.is_valid():
            return redirect('apps.niobe.views.subject_show', course_id = course.id, subject_id = subject.id)
        else:
            kwargs = {'course': course,
                      'subject': subject,
                      'subject_to_teacher_formset': subject_to_teacher_formset}
            return render_to_response('niobe/subject_show.html',
                                      kwargs,
                                      context_instance = RequestContext(request))

# --- Teachers ---

@login_required
def teacher_list(request):
    teachers = Teacher.objects.all()
    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            name = search_form.cleaned_data['name']
            teachers = teachers.filter(user_profile__user__last_name__istartswith = name, user_profile__user__is_active=True)
    else:
        search_form = SearchForm()

    kwargs = {'teachers_page': make_page(request, teachers),
              'search_form': search_form}
    return render_to_response('niobe/teacher_list.html',
                              kwargs,
                              context_instance = RequestContext(request))