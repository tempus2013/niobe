��            )   �      �     �     �     �     �  
   �  1   �  	         
               (     8     T     o  Z   v     �     �  	   �     �  &   �          %     -     6     H     M     Z     _  �  d               4     =     C  I   I     �     �     �     �     �     �     �     �  p        w          �  	   �  1   �  
   �  
   �     �     �            	        "                                             	                   
                                                                          Choose Correct errors listed below. Degree Delete First name Internal error, please contact the Administrator. Last name Major Module Number of groups Number of hours Save and return to the list Save and stay on this page Search Selected subject does not exist. If you see this message again, contact the Administrator. Sem/Year Semester Specialty Subject Subject %s has been saved succesfully. Subjects Teacher Teachers Teachers Workload Type Type of pass Unit Year Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-26 14:30+0200
PO-Revision-Date: 2013-04-26 14:40+0100
Last-Translator: Damian Rusinek <damian.rusinek@umcs.lublin.pl>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Wybierz Popraw poniższe błędy. Stopień Usuń Imię Wewnętrzny Błąd Serwera, proszę skontaktować się z Administratorem. Nazwisko Kierunek Moduł Liczba grup Liczba godzin Zapisz i wróć do listy Zapisz i zostań na tej stronie Szukaj Wybrany przedmiot nie istnieje. Jeśli ta informacja będzie się powtarzać, skontaktuj się z Administratorem. Sem/Rok Semestr Specjalność Przedmiot Przedmiot '%s' został zaktualizowany pomyślnie. Przedmioty Nauczyciel Nauczyciele Obsada Zajęć Typ Rodzaj zaliczenia Jednostka Rok 